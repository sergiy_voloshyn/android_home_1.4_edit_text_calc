package com.example.user.home_1_4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/*
4 Написать калькулятор. Есть два текстовых поля для ввода чисел (EditText) и четыре кнопки арифметических действий (+, -, *, /).
 Результат вывести в TextView.
4.1* Заменить два текстовых поля одним, в котором будет введено полностью выражение (например "2+2" или "16/4").
 Добавить одну кнопку для расчета и вывода результата.

 */
public class MainActivity extends AppCompatActivity {

    EditText number1;
    EditText number2;
    Button sumButton;
    Button subButton;
    Button mulButton;
    Button divButton;
    TextView resView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        number1 = (EditText) findViewById(R.id.editTextNumber1);
        number2 = (EditText) findViewById(R.id.editTextNumber2);
        sumButton = (Button) findViewById(R.id.buttonSum);
        subButton = (Button) findViewById(R.id.buttonSub);
        mulButton = (Button) findViewById(R.id.buttonMul);
        divButton = (Button) findViewById(R.id.buttonDiv);
        resView = (TextView) findViewById(R.id.textViewRes);


        sumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = 0;
                int num2 = 0;

                if ((!number1.getText().toString().isEmpty()) & (!number2.getText().toString().isEmpty())) {
                    try {
                        num1 = Integer.parseInt(number1.getText().toString());
                        num2 = Integer.parseInt(number2.getText().toString());
                    } catch (NumberFormatException e) {
                        resView.setText("Convert error!");
                        return;
                    }
                    resView.setText(String.valueOf(num1 + num2));

                }
            }
        });

        subButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = 0;
                int num2 = 0;

                if ((!number1.getText().toString().isEmpty()) & (!number2.getText().toString().isEmpty())) {
                    try {
                        num1 = Integer.parseInt(number1.getText().toString());
                        num2 = Integer.parseInt(number2.getText().toString());
                    } catch (NumberFormatException e) {
                        resView.setText("Convert error!");
                        return;
                    }
                    resView.setText(String.valueOf(num1 - num2));
                }
            }
        });

        mulButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = 0;
                int num2 = 0;

                if ((!number1.getText().toString().isEmpty()) & (!number2.getText().toString().isEmpty())) {
                    try {
                        num1 = Integer.parseInt(number1.getText().toString());
                        num2 = Integer.parseInt(number2.getText().toString());
                    } catch (NumberFormatException e) {
                        resView.setText("Convert error!");
                        return;
                    }
                    resView.setText(String.valueOf(num1 * num2));

                }
            }
        });

        divButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1 = 0;
                int num2 = 0;

                if ((!number1.getText().toString().isEmpty()) & (!number2.getText().toString().isEmpty())
                        & !number2.getText().toString().equals("0")) {
                    try {
                        num1 = Integer.parseInt(number1.getText().toString());
                        num2 = Integer.parseInt(number2.getText().toString());
                    } catch (NumberFormatException e) {
                        resView.setText("Convert error!");
                        return;
                    }
                    resView.setText(String.valueOf(num1 / num2));

                }
            }
        });


    }


}
